variable "prefix" {
  # A personnaliser avec votre nom
  default = "apiquard"
}

variable "environment" {
  default = "demo"
}

variable "project" {
  default = "MEWO"
}

variable "ssh_key" {
  default = ""
}