provider "azurerm" {
  features {}
}

# State Backend
terraform {
  backend "azurerm" {
    resource_group_name   = "tfstate"
    storage_account_name  = "tfstatepiquard"
    container_name        = "tfstate"
    key                   = "apiquard.terraform.tfstate"

  }
}
